require 'test_helper'

class PhysicianStatusesControllerTest < ActionController::TestCase
  setup do
    @physician_status = physician_statuses(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:physician_statuses)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create physician_status" do
    assert_difference('PhysicianStatus.count') do
      post :create, physician_status: { status_name: @physician_status.status_name }
    end

    assert_redirected_to physician_status_path(assigns(:physician_status))
  end

  test "should show physician_status" do
    get :show, id: @physician_status
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @physician_status
    assert_response :success
  end

  test "should update physician_status" do
    patch :update, id: @physician_status, physician_status: { status_name: @physician_status.status_name }
    assert_redirected_to physician_status_path(assigns(:physician_status))
  end

  test "should destroy physician_status" do
    assert_difference('PhysicianStatus.count', -1) do
      delete :destroy, id: @physician_status
    end

    assert_redirected_to physician_statuses_path
  end
end
