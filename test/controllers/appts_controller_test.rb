require 'test_helper'

class ApptsControllerTest < ActionController::TestCase
  setup do
    @appt = appts(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:appts)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create appt" do
    assert_difference('Appt.count') do
      post :create, appt: { apt_date: @appt.apt_date, diagnostic_code_id: @appt.diagnostic_code_id, patient_id: @appt.patient_id, phy_notes: @appt.phy_notes, physician_id: @appt.physician_id, reason: @appt.reason, slot_id: @appt.slot_id }
    end

    assert_redirected_to appt_path(assigns(:appt))
  end

  test "should show appt" do
    get :show, id: @appt
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @appt
    assert_response :success
  end

  test "should update appt" do
    patch :update, id: @appt, appt: { apt_date: @appt.apt_date, diagnostic_code_id: @appt.diagnostic_code_id, patient_id: @appt.patient_id, phy_notes: @appt.phy_notes, physician_id: @appt.physician_id, reason: @appt.reason, slot_id: @appt.slot_id }
    assert_redirected_to appt_path(assigns(:appt))
  end

  test "should destroy appt" do
    assert_difference('Appt.count', -1) do
      delete :destroy, id: @appt
    end

    assert_redirected_to appts_path
  end
end
