class CreateAppointments < ActiveRecord::Migration
  def change
    create_table :appointments do |t|
      t.integer :physician_id
      t.integer :patient_id
      t.integer :diagnostic_code_id
      t.date :date
      t.integer :slot_id
      t.text :reason
      t.text :phy_notes

      t.timestamps
    end
  end
end
