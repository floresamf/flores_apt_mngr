class CreateDiagnosticCodes < ActiveRecord::Migration
  def change
    create_table :diagnostic_codes do |t|
      t.string :dag_code_num
      t.string :diag_code_name
      t.float :diag_cost

      t.timestamps
    end
  end
end
