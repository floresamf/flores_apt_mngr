class CreateSlots < ActiveRecord::Migration
  def change
    create_table :slots do |t|
      t.string :time_block

      t.timestamps
    end
  end
end
