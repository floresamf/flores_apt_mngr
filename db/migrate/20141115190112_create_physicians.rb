class CreatePhysicians < ActiveRecord::Migration
  def change
    create_table :physicians do |t|
      t.string :phy_name
      t.integer :physician_status_id

      t.timestamps
    end
  end
end
