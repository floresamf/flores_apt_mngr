class CreatePhysicianStatuses < ActiveRecord::Migration
  def change
    create_table :physician_statuses do |t|
      t.string :status_name

      t.timestamps
    end
  end
end
