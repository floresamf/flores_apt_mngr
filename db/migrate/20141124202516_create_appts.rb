class CreateAppts < ActiveRecord::Migration
  def change
    create_table :appts do |t|
      t.integer :patient_id
      t.integer :physician_id
      t.date :apt_date
      t.integer :slot_id
      t.text :reason
      t.integer :diagnostic_code_id
      t.text :phy_notes

      t.timestamps
    end
  end
end
