class CreatePatients < ActiveRecord::Migration
  def change
    create_table :patients do |t|
      t.string :pat_f_name
      t.string :pat_l_name
      t.string :pat_email

      t.timestamps
    end
  end
end
