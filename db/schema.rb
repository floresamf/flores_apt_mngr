# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20141124202516) do

  create_table "appointments", force: true do |t|
    t.integer "physician_id"
    t.integer "patient_id"
    t.integer "diagnostic_code_id"
    t.date    "datetime"
    t.text    "reason"
    t.text    "phy_notes"
    t.integer "slot_id"
  end

  create_table "appts", force: true do |t|
    t.integer  "patient_id"
    t.integer  "physician_id"
    t.date     "apt_date"
    t.integer  "slot_id"
    t.text     "reason"
    t.integer  "diagnostic_code_id"
    t.text     "phy_notes"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "diagnostic_codes", force: true do |t|
    t.string   "dag_code_num"
    t.string   "diag_code_name"
    t.float    "diag_cost"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "patients", force: true do |t|
    t.string   "pat_f_name"
    t.string   "pat_l_name"
    t.string   "pat_email"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "physician_statuses", force: true do |t|
    t.string   "status_name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "physicians", force: true do |t|
    t.string   "phy_name"
    t.integer  "physician_status_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "slots", force: true do |t|
    t.string   "time_block"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
