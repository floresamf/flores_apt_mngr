# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
require 'open-uri'
require 'active_record/fixtures'

open("/vagrant/projects/flores_apt_mngr/lib/assets/Physicians_seed.txt") do |physicians|
  physicians.read.each_line do |physician|
    phy_name, physician_status_id = physician.chomp.split("|")
    Physician.create!(:phy_name => phy_name, :physician_status_id => physician_status_id)
  end
end

open("/vagrant/projects/flores_apt_mngr/lib/assets/patients_seed.txt") do |patients|
  patients.read.each_line do |patient|
    pat_f_name, pat_l_name, pat_email = patient.chomp.split("|")
    Patient.create!(:pat_f_name => pat_f_name, :pat_l_name => pat_l_name, :pat_email => pat_email)
  end
end

open("/vagrant/projects/flores_apt_mngr/lib/assets/appointments_seed.txt") do |appointments|
  appointments.read.each_line do |appointment|
    physician_id, patient_id, diagnostic_code_id, date, reason, phy_notes = appointment.chomp.split("|")
    Appointment.create!(:physician_id => physician_id, :patient_id => patient_id, :diagnostic_code_id => diagnostic_code_id, :date => date, :reason => reason, :phy_notes => phy_notes)
  end
end

open("/vagrant/projects/flores_apt_mngr/lib/assets/slots_seed.txt") do |slots|
  slots.read.each_line do |slot|
    time_block = slot
    Slot.create!(:time_block => time_block)
  end
end

open("/vagrant/projects/flores_apt_mngr/lib/assets/diag_codes_seed.txt") do |diagnostic_codes|
  diagnostic_codes.read.each_line do |diagnostic_code|
    dag_code_num, diag_code_name, diag_cost  = diagnostic_code.chomp.split("|")
    DiagnosticCode.create!(:dag_code_num => dag_code_num, :diag_code_name => diag_code_name, :diag_cost => diag_cost)
  end
end

open("/vagrant/projects/flores_apt_mngr/lib/assets/phys_status_seed.txt") do |physician_statuses|
  physician_statuses.read.each_line do |physician_status|
    status_name = physician_status
    PhysicianStatus.create!(:status_name => status_name)
  end
end