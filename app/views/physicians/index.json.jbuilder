json.array!(@physicians) do |physician|
  json.extract! physician, :id, :phy_name, :physician_status_id
  json.url physician_url(physician, format: :json)
end
