json.array!(@appts) do |appt|
  json.extract! appt, :id, :patient_id, :physician_id, :apt_date, :slot_id, :reason, :diagnostic_code_id, :phy_notes
  json.url appt_url(appt, format: :json)
end
