json.array!(@physician_statuses) do |physician_status|
  json.extract! physician_status, :id, :status_name
  json.url physician_status_url(physician_status, format: :json)
end
