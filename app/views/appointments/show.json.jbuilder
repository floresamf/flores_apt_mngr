json.extract! @appointment, :id, :physician_id, :patient_id, :diagnostic_code_id, :date, :reason, :phy_notes, :created_at, :updated_at
