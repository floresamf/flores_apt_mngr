json.array!(@appointments) do |appointment|
  json.extract! appointment, :id, :physician_id, :patient_id, :diagnostic_code_id, :date, :reason, :phy_notes
  json.url appointment_url(appointment, format: :json)
end
