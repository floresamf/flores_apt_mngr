json.array!(@patients) do |patient|
  json.extract! patient, :id, :pat_f_name, :pat_l_name, :pat_email
  json.url patient_url(patient, format: :json)
end
