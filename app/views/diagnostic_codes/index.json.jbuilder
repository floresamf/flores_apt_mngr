json.array!(@diagnostic_codes) do |diagnostic_code|
  json.extract! diagnostic_code, :id, :dag_code_num, :diag_code_name, :diag_cost
  json.url diagnostic_code_url(diagnostic_code, format: :json)
end
