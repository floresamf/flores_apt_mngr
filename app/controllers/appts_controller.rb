class ApptsController < ApplicationController
  before_action :set_appt, only: [:show, :edit, :update, :destroy]

  # GET /appts
  # GET /appts.json
  def index
    @appts = Appt.all.order(:apt_date)
  end

  # GET /appts/1
  # GET /appts/1.json
  def show
  end

  # GET /appts/new
  def new
    @appt = Appt.new
  end


  # GET /appts/1/edit
  def edit
  end

  # POST /appts
  # POST /appts.json
  def create
    @appt = Appt.new(appt_params)

    respond_to do |format|
      if @appt.save
        format.html { redirect_to @appt, notice: 'Appt was successfully created.' }
        format.json { render :show, status: :created, location: @appt }
      else
        format.html { render :new }
        format.json { render json: @appt.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /appts/1
  # PATCH/PUT /appts/1.json
  def update
    respond_to do |format|
      if @appt.update(appt_params)
        format.html { redirect_to @appt, notice: 'Appt was successfully updated.' }
        format.json { render :show, status: :ok, location: @appt }
      else
        format.html { render :edit }
        format.json { render json: @appt.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /appts/1
  # DELETE /appts/1.json
  def destroy
    @appt.destroy
    respond_to do |format|
      format.html { redirect_to appts_url, notice: 'Appt was successfully destroyed.' }
      format.json { head :no_content }
    end
  end



  private
    # Use callbacks to share common setup or constraints between actions.
    def set_appt
      @appt = Appt.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def appt_params
      params.require(:appt).permit(:patient_id, :physician_id, :apt_date, :slot_id, :reason, :diagnostic_code_id, :phy_notes)
    end
end
