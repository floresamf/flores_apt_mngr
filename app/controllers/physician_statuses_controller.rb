class PhysicianStatusesController < ApplicationController
  before_action :set_physician_status, only: [:show, :edit, :update, :destroy]

  # GET /physician_statuses
  # GET /physician_statuses.json
  def index
    @physician_statuses = PhysicianStatus.all
  end

  # GET /physician_statuses/1
  # GET /physician_statuses/1.json
  def show
  end

  # GET /physician_statuses/new
  def new
    @physician_status = PhysicianStatus.new
  end

  # GET /physician_statuses/1/edit
  def edit
  end

  # POST /physician_statuses
  # POST /physician_statuses.json
  def create
    @physician_status = PhysicianStatus.new(physician_status_params)

    respond_to do |format|
      if @physician_status.save
        format.html { redirect_to @physician_status, notice: 'Physician status was successfully created.' }
        format.json { render :show, status: :created, location: @physician_status }
      else
        format.html { render :new }
        format.json { render json: @physician_status.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /physician_statuses/1
  # PATCH/PUT /physician_statuses/1.json
  def update
    respond_to do |format|
      if @physician_status.update(physician_status_params)
        format.html { redirect_to @physician_status, notice: 'Physician status was successfully updated.' }
        format.json { render :show, status: :ok, location: @physician_status }
      else
        format.html { render :edit }
        format.json { render json: @physician_status.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /physician_statuses/1
  # DELETE /physician_statuses/1.json
  def destroy
    @physician_status.destroy
    respond_to do |format|
      format.html { redirect_to physician_statuses_url, notice: 'Physician status was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_physician_status
      @physician_status = PhysicianStatus.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def physician_status_params
      params.require(:physician_status).permit(:status_name)
    end
end
