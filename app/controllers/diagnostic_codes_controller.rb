class DiagnosticCodesController < ApplicationController
  before_action :set_diagnostic_code, only: [:show, :edit, :update, :destroy]

  # GET /diagnostic_codes
  # GET /diagnostic_codes.json
  def index
    @diagnostic_codes = DiagnosticCode.order("dag_code_num").page(params[:page])
  end

  # GET /diagnostic_codes/1
  # GET /diagnostic_codes/1.json
  def show
  end

  # GET /diagnostic_codes/new
  def new
    @diagnostic_code = DiagnosticCode.new
  end

  # GET /diagnostic_codes/1/edit
  def edit
  end

  # POST /diagnostic_codes
  # POST /diagnostic_codes.json
  def create
    @diagnostic_code = DiagnosticCode.new(diagnostic_code_params)

    respond_to do |format|
      if @diagnostic_code.save
        format.html { redirect_to @diagnostic_code, notice: 'Diagnostic code was successfully created.' }
        format.json { render :show, status: :created, location: @diagnostic_code }
      else
        format.html { render :new }
        format.json { render json: @diagnostic_code.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /diagnostic_codes/1
  # PATCH/PUT /diagnostic_codes/1.json
  def update
    respond_to do |format|
      if @diagnostic_code.update(diagnostic_code_params)
        format.html { redirect_to @diagnostic_code, notice: 'Diagnostic code was successfully updated.' }
        format.json { render :show, status: :ok, location: @diagnostic_code }
      else
        format.html { render :edit }
        format.json { render json: @diagnostic_code.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /diagnostic_codes/1
  # DELETE /diagnostic_codes/1.json
  def destroy
    @diagnostic_code.destroy
    respond_to do |format|
      format.html { redirect_to diagnostic_codes_url, notice: 'Diagnostic code was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_diagnostic_code
      @diagnostic_code = DiagnosticCode.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def diagnostic_code_params
      params.require(:diagnostic_code).permit(:dag_code_num, :diag_code_name, :diag_cost)
    end
end
