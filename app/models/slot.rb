class Slot < ActiveRecord::Base
  has_many :appts
  belongs_to :patient
end
