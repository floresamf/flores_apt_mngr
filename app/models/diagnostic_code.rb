class DiagnosticCode < ActiveRecord::Base
  has_many :appts
  belongs_to :patient

  validates_numericality_of :diag_cost
  validates_presence_of :diag_code_name, :dag_code_num, :diag_cost

end
