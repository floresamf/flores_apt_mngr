class Appt < ActiveRecord::Base
  belongs_to :physician
  belongs_to :patient
  belongs_to :diagnostic_code
  belongs_to :slot

  validates_presence_of :reason, :physician_id, :patient_id, :slot_id
  validates_date :apt_date, :on_or_after => lambda { Date.current}
  validates_associated :patient
  validates_uniqueness_of :slot_id, :scope=>[:physician_id, :apt_date]



end
