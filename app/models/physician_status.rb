class PhysicianStatus < ActiveRecord::Base
  has_many :physicians
end
