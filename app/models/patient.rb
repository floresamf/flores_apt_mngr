class Patient < ActiveRecord::Base
  has_many :appts
  has_many :physicians, through: :appts
  has_many :slots, through: :appts
  has_many :diagnostic_codes, through: :appts

  validates_presence_of :pat_f_name, :pat_l_name, :pat_email
  validates_format_of :pat_email, :with => /\A[^@]+@([^@\.]+\.)+[^@\.]+\z/
  validates_uniqueness_of :pat_email, :case_sensitive => false, :message => "Email is already registered. Please enter a new email"
end
