class Physician < ActiveRecord::Base
  belongs_to :physician_status
  has_many :appts
  has_many :patients, through: :appts

  validates_presence_of :phy_name, :physician_status

  def self.active_physicians
    Physician.where(:physician_status_id => 1 )
  end


end
